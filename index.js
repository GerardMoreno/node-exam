var express = require("express"),
    handlebars = require('express-handlebars');
 
 
var bodyParser = require('body-parser');

 
var app = express();
 
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());     
app.engine('handlebars', handlebars());
app.set('view engine', 'handlebars');
app.set("views", "./views");
 
//EJERCICIO PALABRAS
app.use('/removeduplicatewords', function(req, res){

var words = [];
	//console.log(req.body.palabras);
	for(var i = 0; i < req.body.palabras.split(',').length; ++i){
	words.push(req.body.palabras.split(',')[i]);
	};
	
	for(var i = 0; i < words.length; i++){
	for(var j = i+1; j < words.length; j++){
		if(words[i] == words[j]){
		words.splice(j, j+1);
		}
	}
	}
	var totalword = "";
	for(var i = 0; i < words.length; i++){
		if(i == words.length-1){
		totalword = totalword + words[i];
		}else{
		totalword = totalword + words[i] + ",";
		}
	}
	
	res.end(totalword);
});

const readChunk = require('read-chunk');
const fileType = require('file-type');

//EJERCICIO 2
app.use('/detectfiletype', function(req, res){
	
	var urls = [];

	var url = JSON.stringify(req.body);
	
	for(var i = 0; i < url.split('/').length; ++i){
	urls.push(url.split('/')[i]);
	};
	
	var anotherUrl = url[4];
	console.log(anotherUrl);
	var newUrls = [];
	
	for(var i = 4; i < anotherUrl.split('"').length; ++i){
	newUrls.push(anotherUrl.split('"')[i]);
	};
	
	
	const buffer = readChunk.sync(newUrls[0], 0, 4100);
	
res.end(fileType(buffer));
})

 
app.listen(8000);